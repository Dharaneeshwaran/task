const mongoose=require('mongoose');
const {Schema}=mongoose;
const balancedSchema=new Schema({
	name:String,
	message:String,
	attempt:Number
},{collection:'balanced'});
mongoose.model('balanced',balancedSchema);