const mongoose=require('mongoose');
const express=require('express');
const bodyParser=require('body-parser');
const keys=require('./config/keys');
require('./models/Register');
require('./models/Login');
require('./models/Balanced');
mongoose.connect(keys.mongoURI);

db=mongoose.connection;
db.on ('error', console.error.bind("connection not successed"));
db.once("open", function()
{
	console.log("connection successed");
});
const app=express();
 app.use(bodyParser.urlencoded({
  extended: true
}));
 
app.use(bodyParser.json());


app.get('/',(req,res)=>{

	res.send({Hi:"WELCOME"});
});

app.all('/api/*',function(req,res,next){


	next()
})
require('./passport');
const login=require('./routes/loginAuthRoutes');
app.use('/api',login);

require('./routes/registrationRoutes')(app);


app.listen(5000);
