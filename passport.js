const passport    = require('passport');
const passportJWT = require("passport-jwt");

const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = passportJWT.Strategy;
const mongoose = require('mongoose');
const Register = mongoose.model('register');

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    (username, password, cb) => {
        //Assume there is a DB module providing a global UserModel
        return Register.findOne({username, password})
            .then(register => {
                if (!register) {
                    return cb(null, false, {message: 'Incorrect username or password.'});
                }
                return cb(null, register, {
                    message: 'Logged In Successfully'
                });
            })
            .catch(err => {
            	console.log(err);
                return cb(err);
            });
    }
));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'dharani'
    },
    function (jwtPayload, cb) {
        //return cb(null, 'Authendicated');
       // find the user in db if needed
        return Register.findOne({_id:jwtPayload._id})
            .then(register => {
                return cb(null, register);
            })
            .catch(err => {
                return cb(err);
            });
    }
));